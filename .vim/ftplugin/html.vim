" HTML-specific settings

" Better indentation rules
" set ft=xml

set shiftwidth=2
set tabstop=2

" Different colorschemes
noremap <F2>	:colorscheme evening<cr>
inoremap <F2>	:colorscheme evening<cr>
noremap <F3>	:colorscheme peachpuff<cr>
inoremap <F3>	:colorscheme peachpuff<cr>

command! Opera :!opera %<cr>
command! Firefox :!firefox %<cr>
command! Links :!links -force-html %<cr>

