" Python-specific config settings

" Smart indent - don't remove indent on #
set si  " should already be on from vimrc
inoremap # X#

set expandtab   " Change tabs to spaces

set textwidth=80
set wrap
set linebreak
set nolist

