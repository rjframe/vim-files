" Filetype plugins:
"   python.vim


set nocompatible
filetype plugin indent on
syntax on
set backspace=indent,eol,start
set autoread
set cpoptions+=n

" Smart indent - don't remove indent on #
set si

set title
set mouse=a
set laststatus=2	" Always show status line

" Searching
set hlsearch
set smartcase

"Save on ':q', etc
set autowriteall 

" filetype plugin indent on
set listchars=trail:-
set cmdheight=2
let &sbr="-> "

" Tab spacing
set shiftwidth=4
set tabstop=4
set smarttab

" Show line numbers - toggle between absolute and relative
set number
map <F12>	:if &number <Bar>
				\set relativenumber <Bar>
			\else <Bar>
				\set number <Bar>
			\endif<cr>

" Give the cursor some room
set scrolloff=5

" Show matching brackets
set showmatch

" Show commands
set sc
set wildmenu

" Persistent undo
set undodir="~/.vim/undo"
set undofile

" For ConqueTerm
let g:ConqueTerm_SessionSupport=0
" let g:ConqueTerm_CWInsert=1

" Color schemes
nmap <F2> :colorscheme murphy<CR>
imap <F2> :colorscheme murphy<CR>
nmap <F3> :colorscheme default<CR>
imap <F3> :colorscheme default<CR>

"== Mappings =="

" Mapping Table:
" <F1>  		[terminal:help]
" <F2>  		:colorscheme murphy
" <F3>  		:colorscheme default
" <F4>  		Three-window code layout w/ terminal
" <F5>  		Online Search
" <F6>  		Online Code Search
" <F7>
" <F8>
" <F9>
" <F10> 		[terminal:menu]
" <F11> 		[terminal:fullscreen]
" <F12>			Toggles absolute/relative line numbering
" <c-h>			<c-w>h		Switch to left window
" <c-j>			<c-w>j		Switch to lower window
" <c-k>			<c-w>k		Switch to upper window
" <c-l>			<c-w>l		Switch to right window
" <c-space>		<c-x><c-o>	Omni-completion shortcut


" Window movements - see mapping table
map <c-h>	<c-w>h
map <c-j>	<c-w>j
map <c-k>	<c-w>k
map <c-l>	<c-w>l

" Omni-completion shortcut
imap <c-space> <c-x><c-o>

" Coding 3 Window layout with cmdline
nmap <F4> <F11>:vsp<CR><c-w>l:sp<CR><c-w>j:ConqueTerm sh<CR><ESC><c-w>h<c-w>k


" TODO: Play with these
" Transferring blocks of text between vim sessions (alexyeh)
" nmap xr   :r $HOME/.vimxfer
" nmap xw   :'a,.w! $HOME/.vimxfer
" vmap xw   :w! $HOME/.vimxfer
" nmap xa   :'a,.w>> $HOME/.vimxfer
" vmap xa   :w>> $HOME/.vimxfer
" nmap xy   :'a,.y *
" vmap xy   :y *


" == Functions == "

function! SearchCode()
    let s:browser = "opera"
    let s:wordUnderCursor = expand("<cword>") 

    if &ft == "cpp" || &ft == "c" || &ft == "ruby" || &ft == "python" 
        let s:url = "http://www.koders.com/default.aspx?s=".s:wordUnderCursor."+la=".&ft
    else 
        let s:url = "http://www.koders.com/default.aspx?s=".s:wordUnderCursor
    endif 

    let s:cmd = "silent !" . s:browser . " " . s:url 
    execute s:cmd 
    redraw!
endfunction 

" General search - still check for code-related searches though
function! SearchOnline()
    let s:browser = "opera"
    let s:wordUnderCursor = expand("<cword>") 

    if &ft == "cpp" || &ft == "c" || &ft == "ruby" || &ft == "php" || &ft == "python" 
        let s:url = "http://www.duckduckgo.com/?q=".s:wordUnderCursor."+lang:".&ft
    else 
        let s:url = "http://www.duckduckgo.com/?q=".s:wordUnderCursor
    endif 

    let s:cmd = "silent !" . s:browser . " " . s:url 
    execute s:cmd 
    redraw!
endfunction 

" Mapped functions	- also see full mapping table
nmap <F5>   :call SearchOnline()<cr>
imap <F5>   <esc>:call SearchOnline()<cr>
nmap <F6>   :call SearchCode()<cr>
imap <F6>   <esc>:call SearchCode()<cr>

